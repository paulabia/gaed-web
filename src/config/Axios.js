import axios from 'axios';
import config from './Config';

/**
 * Instância do axios com as credenciais necessárias para requisições na API.
 * Importar sempre essa instância para qualquer requisições.
 */
const instance = axios.create({
  baseURL: config.baseUrl,
  timeout: 1000,
  headers: { Authorization: 'Basic ' + btoa(config.clientId + ':' + config.clientSecret) },
});

export default instance;
