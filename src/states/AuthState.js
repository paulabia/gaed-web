import { observable, useStrict, action, runInAction } from 'mobx';

import history from '../config/history';
import AuthService from '../services/AuthService';

useStrict(true);

class AuthState {
  @observable usuarioLogado = '';
  @observable error = '';
  @observable username = '';

  /**
   * Login handler
   * @param credentials
   */
  @action
  onLogin(credentials) {
    AuthService.getToken(credentials)
      .then(response => {
        runInAction('Load token', () => {
          const { access_token, refresh_token } = response.data;
          this.username = credentials.login;
          AuthService.saveTokens(access_token, refresh_token);
          history.push('/home');
        });
      })
      .catch(response => {
        this._loginError(response);
      });
  }

  @action
  getInfoUser() {
    AuthService.getInfoUser(this.username)
      .then(response => {
        runInAction('Login Sucess', () => {
          localStorage.setItem('usuario', JSON.stringify(response.data));
          this.usuarioLogado = response.data;
        });
      })
      .catch(response => {
        this._loginError(response);
      });
  }

  @action
  _loginError(response) {
    this.clear();
    this.error = response.data;
  }

  @action
  onLogout() {
    this.clear();
    AuthService.logout();
    history.push('/login');
  }

  @action
  onRefreshToken(params) {
    AuthService.refreshToken(params);
  }

  @action
  onLocalLogin() {
    AuthService.localLogin();
    let user = JSON.parse(localStorage.getItem('usuario'));
    if (user) {
      this.usuarioLogado = user;
    }
  }

  @action
  clear() {
    this.usuario = '';
  }

  isLogado() {
    return AuthService.isLogado();
  }
}

export default new AuthState();
