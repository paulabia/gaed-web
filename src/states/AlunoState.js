import { observable, useStrict, action, runInAction } from 'mobx';
import _isEmpty from 'lodash/isEmpty';

import Messagem from '../components/util/Mensagem';

import Aluno from '../domain/usuarios/Aluno';
import AlunoService from '../services/AlunoService';

import LogradouroState from './LogradouroState';

useStrict(true);

class AlunoState {
  @observable alunos = [];
  @observable novoAluno = '';

  @action
  initializeUsuario(params) {
    let id = params != null ? params.id : null;

    if (id != null) {
      console.log('tem id');
    } else {
      this.novoAluno = new Aluno();
    }
  }

  @action
  limparListaAlunos() {
    this.alunos = [];
  }

  @action
  findAlunos() {
    AlunoService.findAll()
      .then(response => {
        runInAction('Load Alunos', () => {
          if (!_isEmpty(response.data)) {
            this.alunos = response.data.map(obj => new Aluno(obj));
          }
        });
      })
      .catch(error => {
        runInAction('Error Alunos', () => {
          console.log(error);
        });
      });
  }

  @action
  updateAlunoAttributes(attrName, value) {
    runInAction('Update novoAluno Attribute', () => {
      this.newFunction(attrName, value);
    });
  }

  @action
  salvar(history) {
    this.novoAluno.endereco = LogradouroState.endereco;
    AlunoService.salvar(JSON.stringify(this.novoAluno))
      .then(response => {
        runInAction('Save Aluno', () => {
          console.log(response.data);
          history.push('/alunos');
        });
      })
      .catch(error => {
        runInAction('Error Aluno', () => {
          Messagem.addMessage('error', 'Erro', 'Ocorreu um erro ao tentar cadastrar.', true);
          console.log(error);
        });
      });
  }

  newFunction(attrName, value) {
    this.novoAluno[attrName] = value;
  }
}

export default new AlunoState();
