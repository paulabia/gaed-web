import { observable, useStrict, action, runInAction } from 'mobx';

useStrict(true);

class MenuState {
  @observable isClosed = true;
  @observable inFocus = false;

  @action
  toggleMenu(hasFocus) {
    runInAction('Toggle Menu', () => {
      this.inFocus = hasFocus;
      this.isClosed = !this.isClosed;

      if (this.isClosed) {
        this.inFocus = false;
      }
    });
  }
}

export default new MenuState();
