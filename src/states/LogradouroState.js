import { observable, useStrict, action, runInAction } from 'mobx';

import Endereco from '../domain/Endereco';

import LogradouroService from '../services/LogradouroService';

useStrict(true);

class LogradouroState {
  @observable endereco;

  @action
  findLogradouro(cep) {
    LogradouroService.findLogradouro(cep)
      .then(response => {
        runInAction('Load Logradouro', () => {
          let obj = response.data;
          this.endereco = new Endereco(obj.id, obj.logradouro, obj.complemento, obj.bairro, obj.uf, null);
        });
      })
      .catch(error => {
        runInAction('Error Logradouro', () => {
          console.log(error);
        });
      });
  }

  @action
  updateEnderecoAttributes(attrName, value) {
    runInAction('Update Endereco Attribute', () => {
      this.endereco[attrName] = value;
    });
  }
}

export default new LogradouroState();
