import { observable } from 'mobx';

export default class Endereco {
  @observable logradouroId = '';
  @observable logradouro = '';
  @observable numero = '';
  @observable complemento = '';
  @observable bairro = '';
  @observable uf = '';

  constructor(id, logradouro, complemento, bairro, uf) {
    this.logradouroId = id;
    this.logradouro = logradouro;
    this.complemento = complemento;
    this.bairro = bairro;
    this.uf = uf;
  }
}
