import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import classNames from 'classnames';
import { observer } from 'mobx-react';

import Header from '../components/Header';
import Menu from '../components/Menu';
import menuState from '../states/MenuState';

@observer
class Template extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    const classMenu = classNames('g-inner-menu sv-pv--20', {
      'g-oculta-menu': !menuState.isClosed,
      active: !menuState.isClosed,
    });

    return (
      <div className="sv-row sv-ma--0">
        <div className="g-header sv-bg-color--blue-800">
          <Header />
        </div>
        <div className="g-body">
          <aside className={classMenu}>
            <Menu />
          </aside>
          <section className="g-inner-content sv-pa--20">{this.props.children}</section>
        </div>
      </div>
    );
  }
}

Template.displayName = 'Template';

Template.propTypes = {
  children: PropTypes.any,
};

export default withRouter(Template);
