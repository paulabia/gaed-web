import React from 'react';
import { action } from 'mobx';

import AlunoSate from '../../states/AlunoState';

import UsuarioFormDefault from '../../components/usuarios/UsuarioFormDefault';

export default class AlunoFormPage extends React.Component {
  constructor(props) {
    super(props);

    this.store = AlunoSate;
  }

  @action
  _handleProp = e => {
    this.store.updateAlunoAttributes(e.target.name, e.target.value);
  };

  @action
  _handleSalvar = e => {
    e.preventDefault();
    let history = this.props.history;
    this.store.salvar(history);
  };

  render() {
    return (
      <div>
        <div className="sv-row">
          <ul className="sv-breadcrumb">
            <li>
              <a href="#">
                <i className="fa fa-home fa-fw" />
              </a>
            </li>
            <li>
              <a href="#">Usuários</a>
            </li>
            <li>
              <a href="/alunos">Alunos</a>
            </li>
            <li>
              <a href="#">Cadastro</a>
            </li>
          </ul>
        </div>

        <hr />

        <form className="sv-form sv-compact">
          <fieldset className="bordered sv-bg-color--steel-50">
            <legend>Cadastro de Aluno</legend>

            <UsuarioFormDefault state={this.store} />
            <div className="sv-row--with-gutter">
              <div className="sv-column">
                <label>
                  <span>Escola Anterior</span>
                  <input
                    type="text"
                    name="escolaAnterior"
                    onChange={e => {
                      this._handleProp(e);
                    }}
                  />
                </label>
              </div>
              <div className="sv-column">
                <label>
                  <span>Série Anterior</span>
                  <input
                    type="text"
                    name="serieAtual"
                    onChange={e => {
                      this._handleProp(e);
                    }}
                  />
                </label>
              </div>
            </div>
          </fieldset>
          <div className="sv-form-action">
            <button className="sv-button out-info">cancelar</button>
            <button className="sv-button info sv-ml--15" onClick={e => this._handleSalvar(e)}>
              salvar
            </button>
          </div>
        </form>
      </div>
    );
  }
}
