import { observable } from 'mobx';

import Usuario from './Usuario';

import isBlank from '../../components/util/utilitario';

export default class Aluno extends Usuario {
  @observable escolaAnterior = '';
  @observable serieAnterior = '';
  @observable escolaAtual = '';
  @observable serieAtual = '';

  constructor(data) {
    super(data);
    if (data) {
      this.escolaAnterior = data.escolaAnterior;
      this.escolaAtual = data.escolaAtual;
    }
  }

  validate() {
    let errors = [];

    isBlank(this.escolaAnterior) ? errors.push('Escola Anterior é de preenchimento obrigatório') : null;
    isBlank(this.serieAnterior) ? errors.push('Série Anterior é de preenchimento obrigatório') : null;

    return errors;
  }
}
