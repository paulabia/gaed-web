import { observable } from 'mobx';
import moment from 'moment';

import Endereco from '../../domain/Endereco';

export default class Usuario {
  @observable id = null;
  @observable nome = '';
  @observable sobreNome = '';
  @observable cpf = '';
  @observable rg = '';
  @observable telefone = '';
  @observable dataNascimento = '';
  @observable sexo = '';
  @observable email = '';
  @observable endereco = '';

  constructor(data) {
    if (data) {
      this.id = data.id;
      this.nome = data.nome;
      this.sobreNome = data.sobreNome;
      this.cpf = data.cpf;
      this.rg = data.rg;
      this.telefone = data.telefone;
      this.dataNascimento = moment(new Date(data.dataNascimento)).format('DD/MM/YYYY');
      this.sexo = data.sexo;
      this.email = data.email;
      this.endereco = new Endereco(
        data.endereco.id,
        data.endereco.logradouro.logradouro,
        data.endereco.compplemento,
        data.endereco.logradouro.bairro,
        data.endereco.logradouro
      );
    }
  }

  toObject() {
    return JSON.parse(JSON.stringify(this));
  }
}
