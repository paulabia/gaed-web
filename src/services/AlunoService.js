import instance from '../config/Axios';

const AlunoService = {
  findAll() {
    return instance.get(`http://localhost:9000/gaed/api/aluno/findAll`);
  },

  salvar(aluno) {
    return instance.put('http://localhost:9000/gaed/api/aluno', { aluno });
  },
};

export default AlunoService;
