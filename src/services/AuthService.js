import Axios from '../config/Axios';
import Uri from 'jsuri';

import Config from '../config/Config';
import InterceptorUtil from '../config/InterceptorUtil';

const AuthService = {
  getToken(credentials) {
    return Axios.post(
      '/oauth/token?grant_type=password&username=' + credentials.login + '&password=' + credentials.senha
    );
  },

  getInfoUser(username) {
    if (!username) {
      let usuario = localStorage.getItem('usuario');
      username = JSON.parse(usuario).userName;
    }
    return Axios.get(Config.apiUrl + '/usuario/infoUser/' + username);
  },

  saveTokens(access_token, refresh_token) {
    localStorage.setItem('access_token', access_token);
    localStorage.setItem('refresh_token', refresh_token);

    // Automatically add access token
    var interceptor = Axios.interceptors.request.use(config => {
      config.url = new Uri(config.url).addQueryParam('access_token', access_token);
      return config;
    });

    InterceptorUtil.setInterceptor(interceptor);
  },

  logout() {
    localStorage.clear();
    Axios.interceptors.request.eject(InterceptorUtil.getInterceptor());
  },

  refreshToken(params) {
    let refreshToken = localStorage.getItem('refresh_token');

    if (refreshToken) {
      Axios.interceptors.request.eject(InterceptorUtil.getInterceptor());
      Axios.get('/oauth/token?grant_type=refresh_token&refresh_token=' + refreshToken)
        .then(response => {
          this.saveTokens(response.data);

          // Replay request
          Axios(params.initialRequest)
            .then(response => {
              params.resolve(response);
            })
            .catch(response => {
              params.reject(response);
            });
        })
        .catch(() => {
          this.onLogout();
        });
    }
  },

  localLogin() {
    let accessToken = localStorage.getItem('access_token');
    let refreshToken = localStorage.getItem('refresh_token');
    let user = JSON.parse(localStorage.getItem('usuario'));
    if (accessToken && refreshToken && user) {
      this.saveTokens(accessToken, refreshToken);
    }
  },

  isLogado() {
    // let accessToken = localStorage.getItem('access_token');
    // let refreshToken = localStorage.getItem('refresh_token');
    // let user = JSON.parse(localStorage.getItem('usuario'));
    // if (accessToken && refreshToken && user) {
    //   return true;
    // } else {
    //   return false;
    // }
    return true;
  },
};

export default AuthService;
