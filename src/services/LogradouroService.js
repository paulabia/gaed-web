import Axios from '../config/Axios';

const LogradouroService = {
  findLogradouro(cep) {
    return Axios.get(`http://localhost:9000/gaed/api/logradouro/${cep}`);
  },
};

export default LogradouroService;
