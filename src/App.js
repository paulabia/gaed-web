import React from 'react';
import { Switch } from 'react-router-dom';
import { Router, Route, Redirect } from 'react-router';

import browserHistory from './config/history';

import Template from './pages/Template';
import Home from './pages/Home';
import Login from './pages/Login';
import Notas from './pages/Notas';
import Ocorrencia from './pages/Ocorrencia';
import Tarefas from './pages/Tarefas';
import AlunoListPage from './pages/usuarios/AlunoListPage';
import AlunoFormPage from './pages/usuarios/AlunoFormPage';

import AuthState from './states/AuthState';

let page = component => {
  return AuthState.isLogado() ? component : <Redirect to="/login" />;
};

const App = () => (
  <Router history={browserHistory}>
    <Switch>
      <Route component={Login} exact path="/login" />
      <Route exact path="/" render={() => <Redirect to="/login" />} />
      <Template>
        <Route exact path="/home" render={() => page(<Home />)} />
        <Route path={`/notas`} render={() => page(<Notas />)} />
        <Route path={`/ocorrencia`} render={() => page(<Ocorrencia />)} />
        <Route path={`/tarefas`} render={() => page(<Tarefas />)} />
        <Route path={`/alunos/`} render={() => page(<AlunoListPage />)} />
        <Route path={`/aluno/cadastro`} render={() => page(<AlunoFormPage />)} />
      </Template>
    </Switch>
  </Router>
);

App.displayName = 'App';

export default App;
