import React from 'react';
import { observer } from 'mobx-react';
import { action } from 'mobx';
import InputMask from 'react-input-mask';

import LogradouroState from '../states/LogradouroState';

@observer
export default class Endereco extends React.Component {
  constructor(props) {
    super(props);

    this.store = LogradouroState;

    this.state = { disable: true };
  }

  _handleCep = e => {
    let cep = e.target.value;
    let patternCep = /^[0-9]{5}-[0-9]{3}$/;
    if (patternCep.test(cep)) {
      this.store.findLogradouro(cep);
      this.setState({ disable: false });
    }
  };

  @action
  _handleProp = e => {
    this.store.updateEnderecoAttributes(e.target.name, e.target.value);
  };

  render() {
    return (
      <div>
        <div className="sv-row--with-gutter">
          <div className="sv-column">
            <label>
              <span data-info="(9 caracters)">CEP</span>
              <InputMask type="text" name="cep" mask="99999-999" onChange={this._handleCep} />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Logradouro</span>
              <input
                type="text"
                name="logradouro"
                value={this.store.endereco ? this.store.endereco.logradouro : ''}
                onChange={e => {
                  this._handleProp(e);
                }}
                disabled={this.state.disable}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Bairro</span>
              <input
                type="text"
                name="bairro"
                value={this.store.endereco ? this.store.endereco.bairro : ''}
                onChange={e => {
                  this._handleProp(e);
                }}
                disabled={this.state.disable}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Uf</span>
              <input
                type="text"
                name="uf"
                value={this.store.endereco ? this.store.endereco.uf : ''}
                onChange={e => {
                  this._handleProp(e);
                }}
                disabled={this.state.disable}
              />
            </label>
          </div>
        </div>
        <div className="sv-row--with-gutter">
          <div className="sv-column">
            <label>
              <span>Numero</span>
              <input
                type="text"
                name="numero"
                onChange={e => {
                  this._handleProp(e);
                }}
                disabled={this.state.disable}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Complemento</span>
              <input
                type="text"
                name="complemento"
                onChange={e => {
                  this._handleProp(e);
                }}
                disabled={this.state.disable}
              />
            </label>
          </div>
        </div>
      </div>
    );
  }
}
