import React from 'react';
import classNames from 'classnames';
import { observer } from 'mobx-react';

import Avatar from './Avatar';
import IconSort from '../IconeSort';

import AuthState from '../../states/AuthState';

@observer
export default class AvatarUser extends React.Component {
  constructor(props) {
    super(props);

    this.store = AuthState;

    this.handleLogout = this.handleLogout.bind(this);

    this.state = {
      open: false,
    };
  }

  componentDidMount() {
    if (AuthState.username) {
      this.store.getInfoUser();
    }
  }

  handlePerfil = () => {
    this.setState({ open: !this.state.open });
  };

  handleLogout = () => {
    this.store.onLogout();
  };

  render() {
    let classHidden = classNames({ 'sv-hidden': !this.state.open });
    return (
      <div>
        <div className="sv-row">
          <div className="sv-column sv-ml--5 sv-mt--5 ga-user">
            <Avatar
              img="http://www.gravatar.com/avatar/a16a38cdfe8b2cbd38e8a56ab93238d3"
              name={this.store.usuarioLogado.nome}
            />
          </div>
          <div
            className="sv-column sv-ml--10 sv-mt--15 sv-pointer ga-write ga-user-nome "
            onClick={() => {
              this.handlePerfil();
            }}
          >
            <span>{this.store.usuarioLogado.nome} </span>
            <IconSort open={this.state.open} />
          </div>
        </div>

        <div className={classHidden}>
          <div className="sv-row ga-user-details">
            <div className="ga-arrow-up" />
            <div className="ga-user-info-details">
              <h6>{this.store.usuarioLogado.nome}</h6>
              <span>
                <button className="sv-button link link-info small" onClick={() => this.handleLogout()}>
                  Sair
                </button>
              </span>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
