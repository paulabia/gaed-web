import React from 'react';
import PropTypes from 'prop-types';

const Avatar = ({ img, name }) => {
  return <img className="ga-user-img" src={img} alt={name} />;
};

Avatar.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,
};

export default Avatar;
