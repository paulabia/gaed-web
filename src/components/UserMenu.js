import React from 'react';
import { NavLink } from 'react-router-dom';

const UserMenu = () => {
  return (
    <div>
      <li className="g-user sv-pointer">
        <span>Usuários</span>
        <i className="fa fa-users fa-lg" aria-hidden="true" />
      </li>

      <ul className="g-sub-menu sv-pull-right">
        <NavLink to={`/alunos`} activeClassName="selected" exact>
          <li>
            <span>Aluno</span>
            <i className="fa fa-user" aria-hidden="true" />
          </li>
        </NavLink>
        <NavLink to={`/professores`} activeClassName="selected" exact>
          <li>
            <span>Professor</span>
            <i className="fa fa-user" aria-hidden="true" />
          </li>
        </NavLink>
        <NavLink to={`/responsaveis`} activeClassName="selected" exact>
          <li>
            <span>Responsável</span>
            <i className="fa fa-user" aria-hidden="true" />
          </li>
        </NavLink>
        <NavLink to={`/secretarias`} activeClassName="selected" exact>
          <li>
            <span>Secretária</span>
            <i className="fa fa-user" aria-hidden="true" />
          </li>
        </NavLink>
        <NavLink to={`/gestores`} activeClassName="selected" exact>
          <li>
            <span>Gestor</span>
            <i className="fa fa-user" aria-hidden="true" />
          </li>
        </NavLink>
      </ul>
    </div>
  );
};

export default UserMenu;
