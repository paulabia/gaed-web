import React from 'react';
import PropTypes from 'prop-types';

export default function Select({ options, actionChange, name }) {
  let optionsSelect = options.map((option, idx) => {
    return (
      <option key={idx} value={option}>
        {option}
      </option>
    );
  });

  return (
    <div className="sv-select">
      <select onChange={actionChange} name={name}>
        <option value="">Selecione</option>
        {optionsSelect}
      </select>
      <label>
        <i className="fa fa-angle-down fa-fw" />
      </label>
    </div>
  );
}
Select.propTypes = {
  options: PropTypes.array,
  name: PropTypes.string,
  actionChange: PropTypes.func.isRequired,
};
