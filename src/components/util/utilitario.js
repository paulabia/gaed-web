import PropTypes from 'prop-types';
import _isEmpty from 'lodash/isEmpty';
import _isString from 'lodash/isString';
import _trim from 'lodash/trim';

export const isBlank = pString => {
  return _isString(pString) ? !!_trim(pString) : _isEmpty(pString);
};

isBlank.PropTypes = {
  pString: PropTypes.string.isRequired,
};
