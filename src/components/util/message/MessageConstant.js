const MessageConstant = {
  allowedContexts: ['info', 'error', 'success', 'warning'],
  allowedPositions: ['top', 'bottom'],
};

export default MessageConstant;
