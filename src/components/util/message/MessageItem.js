import React from 'react';
import PropTypes from 'prop-types';
import MessageApi from './MessageApi';
import MessageStyles from './MessageStyles';
import classNames from 'classnames';

const HermesMessageItem = ({ message }) => {
  const classButtonMap = {
    info: 'link-info',
    error: 'link-danger',
    success: 'link-primary',
    warning: 'link-warning',
  };

  let classes = classNames('sv-button link', classButtonMap[MessageApi.context]);

  let deleteButton = message.isDeletable ? (
    <button className={classes} onClick={() => MessageApi.removeMessage(message.id)}>
      &times;
    </button>
  ) : (
    ''
  );

  return (
    <li style={MessageStyles.msgStyle}>
      {deleteButton}
      {message.text}
    </li>
  );
};

HermesMessageItem.propTypes = {
  message: PropTypes.object,
};

HermesMessageItem.displayName = 'HermesMessageItem';

export default HermesMessageItem;
