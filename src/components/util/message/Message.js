import React from 'react';
import PropTypes from 'prop-types';
import Update from 'react-addons-update';
import MessageApi from './MessageApi';
import MessageItem from './MessageItem';
import MessageStyles from './MessageStyles';
import MessageConstantes from './MessageConstant';
import uuid from 'uuid';
import classNames from 'classnames';

var _hideTimeOut;
class Message extends React.Component {
  static addMessage(msg, isDeletable = false) {
    MessageApi.addMessage(uuid.v1(), msg, isDeletable);
  }

  static setContext(newContext) {
    MessageApi.setContext(newContext);
  }

  static setPosition(newPosition) {
    MessageApi.setPosition(newPosition);
  }

  static setTitle(newTitle) {
    MessageApi.setTitle(newTitle);
  }

  static clearMessages() {
    MessageApi.clearMessages();
  }

  constructor(props) {
    super(props);

    this.state = {
      autoClose: this.props.autoClose,
      autoCloseInMiliseconds: this.props.autoCloseInMiliseconds,
      context: this.props.context,
      messages: [],
      position: this.props.position,
      title: this.props.title,
      visible: false,
    };

    this.onStackChange = this.onStackChange.bind(this);
  }

  componentDidMount() {
    MessageApi.addChangeListener(this.onStackChange);
  }

  componentWillUnmount() {
    MessageApi.removeChangeListener(this.onStackChange);
  }

  onStackChange() {
    let newState = Update(this.state, {
      context: { $set: MessageApi.context || this.props.context },
      messages: { $set: MessageApi.messages },
      position: { $set: MessageApi.position || this.props.position },
      title: { $set: MessageApi.title || this.props.title },
      visible: { $set: MessageApi.messages.length },
    });
    this.setState(newState);
    if (_hideTimeOut) {
      clearTimeout(_hideTimeOut);
    }

    _hideTimeOut = setTimeout(() => {
      if (this.state.autoClose && this.state.visible) {
        this.hide();
      }
    }, this.state.autoCloseInMiliseconds);
  }

  hide() {
    clearTimeout(_hideTimeOut);
    let newState = Update(this.state, {
      visible: { $set: false },
    });
    MessageApi.clearMessages();
    this.setState(newState);
  }

  render() {
    let createMessage = function(message) {
      return <MessageItem key={message.id} message={message} />;
    };

    let visible = this.state.visible ? 'block' : 'none';
    const display = { display: visible };
    let cssClasses = classNames('sv-messagebox sv-no-margins', this.state.context);
    const styles = Object.assign({}, display, MessageStyles[this.state.position]);

    return (
      <div className={cssClasses} style={styles}>
        <button className="sv-messagebox__close" onClick={() => this.hide()}>
          &times;
        </button>
        <header>
          <h6>{this.state.title}</h6>
        </header>
        <main>
          <ul style={MessageStyles.msgListStyle}>{this.state.messages.map(createMessage)}</ul>
        </main>
      </div>
    );
  }
}

Message.defaultProps = {
  autoClose: true,
  autoCloseInMiliseconds: 10000,
  context: 'info',
  position: 'bottom',
  title: '',
};

Message.propTypes = {
  autoClose: PropTypes.bool,
  autoCloseInMiliseconds: PropTypes.number,
  context: PropTypes.oneOf(MessageConstantes.allowedContexts).isRequired,
  position: PropTypes.oneOf(MessageConstantes.allowedPositions),
  title: PropTypes.string,
};

Message.displayName = 'Hermes';

export default Message;
