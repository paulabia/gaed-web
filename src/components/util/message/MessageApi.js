import { EventEmitter } from 'events';
import MessageConstantes from './MessageConstant';

const messageApi = {
  ...EventEmitter.prototype,
  messages: [],
  title: '',
  context: '',
  position: '',

  addMessage(id, message, isDeletable) {
    if (this.getIndex(id) === -1) {
      this.messages.push({ id: id, text: message, isDeletable: isDeletable });
      this.emitChange();
    }
  },

  removeMessage(id) {
    if (this.getIndex(id) !== -1) {
      this.messages.splice(this.getIndex(id), 1);
      this.emitChange();
    }
  },

  clearMessages() {
    this.messages = [];
    this.emitChange();
  },

  setContext(context) {
    if (MessageConstantes.allowedContexts.indexOf(context) >= 0) {
      this.context = context;
      this.emitChange();
    } else {
      throw TypeError(
        `${context} is not allowed, context should be on of ${MessageConstantes.allowedContexts.toString()}.`
      );
    }
  },

  setPosition(position) {
    if (MessageConstantes.allowedPositions.indexOf(position) >= 0) {
      this.position = position;
      this.emitChange();
    } else {
      throw TypeError(
        `${position} is not allowed, position should be on of ${MessageConstantes.allowedPositions.toString()}.`
      );
    }
  },

  setTitle(title) {
    this.title = title;
    this.emitChange();
  },

  getIndex(id) {
    return this.messages.findIndex(loader => loader.id === id);
  },

  emitChange() {
    this.emit('change');
  },

  addChangeListener(callback) {
    this.on('change', callback);
  },

  removeChangeListener(callback) {
    this.removeListener('change', callback);
  },
};

export default messageApi;
