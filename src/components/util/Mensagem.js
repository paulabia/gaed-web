import Message from './message/Message';

const Messagem = {
  addMessage(contextMessage, messageTitle, message, clearMessages) {
    clearMessages ? this.clearMessages() : null;
    Message.setContext(contextMessage);
    Message.setTitle(messageTitle);
    Message.addMessage(message, true);
  },
  clearMessages() {
    Message.clearMessages();
  },
};

export default Messagem;
