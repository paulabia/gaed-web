import React from 'react';
import classNames from 'classnames';
import { observer } from 'mobx-react';

import menuState from '../states/MenuState';

import AvatarUser from '../components/usuarioInfo/AvatarUser';

@observer
export default class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const menuTriggerClass = classNames('ga-hamburger hamburger-cancel', {
      active: !menuState.isClosed,
    });

    return (
      <header>
        <div>
          <div className="ga-trigger-menu sv-bg-color--blue-900">
            <button className={menuTriggerClass} onClick={() => menuState.toggleMenu(true)}>
              <span className="icon" />
            </button>
          </div>
          <div className="ga-trigger-nome-projeto sv-bg-color--blue-800">
            <span>GAED</span>
          </div>
          <div className="ga-info-user sv-pull-right">
            <AvatarUser />
          </div>
        </div>
      </header>
    );
  }
}
