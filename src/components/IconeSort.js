import React from 'react';
import PropTypes from 'prop-types';

const iconSort = ({ open }) => {
  return open ? sortAsc : sortDesc;
};

const sortAsc = <i className="fa fa-sort-asc sv-ml--10" aria-hidden="true" />;

const sortDesc = <i className="fa fa-sort-desc sv-ml--10" aria-hidden="true" />;

iconSort.PropTypes = {
  open: PropTypes.bool,
};

iconSort.defaultProps = {
  open: false,
};

export default iconSort;
