import React from 'react';
import { NavLink } from 'react-router-dom';

import UserMenu from './UserMenu';

export default class Menu extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <ul>
          <NavLink to={`/home`} activeClassName="selected" exact>
            <li>
              <span>Home</span>
              <i className="fa fa-home fa-lg" aria-hidden="true" />
            </li>
          </NavLink>

          <NavLink to={`/notas`} activeClassName="selected" exact>
            <li>
              <span>Notas</span>
              <i className="fa fa-file-text-o fa-lg" aria-hidden="true" />
            </li>
          </NavLink>

          <NavLink to={`/tarefas`} activeClassName="selected" exact>
            <li>
              <span>Tarefas</span>
              <i className="fa fa-book fa-lg" aria-hidden="true" />
            </li>
          </NavLink>

          <NavLink to={`/ocorrencia`} activeClassName="selected" exact>
            <li>
              <span>Ocorrência</span>
              <i className="fa fa-file-text fa-lg" aria-hidden="true" />
            </li>
          </NavLink>
        </ul>
        <UserMenu />
      </div>
    );
  }
}
