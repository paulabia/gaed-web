import React from 'react';
import PropTypes from 'prop-types';
import _values from 'lodash/values';
import _isEmpty from 'lodash/isEmpty';
import InputMask from 'react-input-mask';

import Sexo from '../../domain/Sexo';

import Select from '../../components/select';
import Endereco from '../../components/Endereco';

export default class UsuarioFormDefault extends React.Component {
  constructor(props) {
    super(props);

    this.store = props.state;
    this.store.initializeUsuario(null);

    this.state = {
      email: '',
      confirmEmail: '',
      touched: {
        email: false,
        confirmEmail: false,
      },
    };
  }

  _validaEmail() {
    let valid = true;
    const emailPattern = /(.+)@(.+){2,}\.(.+){2,}/;
    if (!_isEmpty(this.state.email) || this.state.touched.email) {
      if (!emailPattern.test(this.state.email)) {
        valid = false;
      }
    }

    return valid;
  }

  _ValidaConfirmEmail() {
    let valid = true;
    if (!_isEmpty(this.state.confirmEmail) || this.state.touched.confirmEmail) {
      if (this.state.confirmEmail != this.state.email) {
        valid = false;
      }
    }

    return valid;
  }

  _handleEmailChange = e => {
    this.setState({ email: e.target.value });
    this.store.updateAlunoAttributes('email', e.target.value);
  };

  _handleConfirmEmailChange = e => {
    this.setState({ confirmEmail: e.target.value });
  };

  _handleBlur = field => e => {
    this.setState({
      touched: { ...this.state.touched, [field]: true },
    });
    console.log(e);
  };

  _handleProp = e => {
    this.store.updateAlunoAttributes(e.target.name, e.target.value);
  };

  _handleDataNascimento = e => {
    let values = e.target.value.split('/');
    let time = new Date(values[2], values[1], values[0]).getTime();
    this.store.updateAlunoAttributes(e.target.name, time);
  };

  render() {
    return (
      <div>
        <div className="sv-row--with-gutter">
          <div className="sv-column">
            <label>
              <span>Nome</span>
              <input
                type="text"
                name="nome"
                onChange={e => {
                  this._handleProp(e);
                }}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>SobreNome</span>
              <input
                type="text"
                name="sobreNome"
                onChange={e => {
                  this._handleProp(e);
                }}
              />
            </label>
          </div>
        </div>
        <div className="sv-row--with-gutter">
          <div className="sv-column">
            <label>
              <span>CPF</span>
              <InputMask
                type="text"
                name="cpf"
                mask="999.999.999-99"
                onChange={e => {
                  this._handleProp(e);
                }}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>RG</span>
              <InputMask
                type="text"
                name="rg"
                mask="99.999.999-9"
                maskChar=" "
                onChange={e => {
                  this._handleProp(e);
                }}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Data Nascimento</span>
              <InputMask
                type="text"
                name="dataNascimento"
                mask="99/99/9999"
                maskChar=" "
                onChange={e => {
                  this._handleProp(e);
                }}
                onBlur={e => {
                  this._handleDataNascimento(e);
                }}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span>Telefone</span>
              <InputMask
                type="text"
                name="telefone"
                mask="(99) 999-99-9999"
                maskChar=" "
                onChange={e => {
                  this._handleProp(e);
                }}
              />
            </label>
          </div>
        </div>
        <div className="sv-row--with-gutter">
          <div className="sv-column">
            <label>
              <span>Sexo</span>
              <div className="sv-select">
                <Select name="sexo" options={_values(new Sexo())} actionChange={this._handleProp} />
              </div>
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span data-error={this._validaEmail() ? '' : 'e-mail Inválido'}>E-mail</span>
              <input
                type="text"
                name="email"
                className={this._validaEmail() ? '' : 'is--invalid'}
                value={this.state.email}
                onChange={this._handleEmailChange}
                onBlur={this._handleBlur('email')}
              />
            </label>
          </div>
          <div className="sv-column">
            <label>
              <span data-error={this._ValidaConfirmEmail() ? '' : 'Valor incorreto'}>Confirme e-mail</span>
              <input
                type="text"
                name="senha"
                className={this._ValidaConfirmEmail() ? '' : 'is--invalid'}
                value={this.state.confirmEmail}
                onChange={this._handleConfirmEmailChange}
                onBlur={this._handleBlur('confirmEmail')}
              />
            </label>
          </div>
        </div>
        <Endereco />
      </div>
    );
  }
}

UsuarioFormDefault.propTypes = {
  store: PropTypes.object,
};
