import React from 'react';
import { observer } from 'mobx-react';
import { Link } from 'react-router-dom';

import AlunoState from '../../states/AlunoState';

@observer
export default class AlunoListPage extends React.Component {
  constructor(props) {
    super(props);
    this.store = AlunoState;
  }

  componentDidMount() {
    AlunoState.findAlunos();
  }

  componentWillMount() {
    AlunoState.limparListaAlunos();
  }

  render() {
    return (
      <div>
        <div className="sv-row">
          <ul className="sv-breadcrumb">
            <li>
              <a href="#">
                <i className="fa fa-home fa-fw" />
              </a>
            </li>
            <li>
              <a href="#">Usuários</a>
            </li>
            <li>
              <a href="#">Aluno</a>
            </li>
          </ul>
        </div>

        <hr />

        <div className="sv-table-responsive-vertical">
          <table className="sv-table with--hover with--borders">
            <caption>
              <div className="sv-row">
                <Link to={`/aluno/cadastro`}>
                  <button className="sv-button primary">
                    <i className="fa fa-plus" aria-hidden="true" /> Novo
                  </button>
                </Link>
              </div>
            </caption>

            <thead>
              <tr>
                <th>Id</th>
                <th>Nome</th>
                <th>RA</th>
                <th>Data Nascimento</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {this.store.alunos.length == 0 ? (
                <tr>
                  <td colSpan="5" className="g-text-alig-center">
                    Nenhum aluno cadastrado.
                  </td>
                </tr>
              ) : (
                this.store.alunos.map(usuario => {
                  return (
                    <tr key={usuario.id}>
                      <td data-title="Id">{usuario.id}</td>
                      <td data-title="Name">
                        {usuario.nome} {usuario.sobreNome}
                      </td>
                      <td data-title="Ra">{usuario.cpf}</td>
                      <td data-title="dataNascimento">{usuario.dataNascimento}</td>
                      <td data-title="Action">
                        <button className="sv-button info small">Editar</button>
                        <button className="sv-button danger small sv-ml--5">Excluir</button>
                      </td>
                    </tr>
                  );
                })
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}
