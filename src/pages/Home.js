import React from 'react';
import UploadSimples from '../components/UploadSimples';

export default class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <p className="sv-fw-bold">Home</p>
        <UploadSimples />
      </div>
    );
  }
}
