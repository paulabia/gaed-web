import React from 'react';

import AuthState from '../states/AuthState';

export default class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      login: '',
      senha: '',
    };
  }

  _handleChange = e => {
    var change = {};
    change[e.target.name] = e.target.value;
    this.setState(change);
  };

  _handleLogin = e => {
    e.preventDefault();
    AuthState.onLogin({ login: this.state.login, senha: this.state.senha }, this.props.history);
  };

  render() {
    return (
      <div>
        <div className="g-body-login sv-pa--00">
          <div className="sv-card sv-pa--15 sv-pt--50 g-card">
            <section>
              <div className="g-header-form">
                <img className="g-user-img-60" src="src/images/user-2.png" alt="login" />
                <h2>Login</h2>
              </div>
            </section>
            <section>
              <form className="sv-form">
                <div className="sv-row">
                  <div className="sv-column">
                    <label>
                      <input
                        type="text"
                        name="login"
                        placeholder="Login"
                        onChange={e => {
                          this._handleChange(e);
                        }}
                      />
                    </label>
                  </div>
                </div>
                <div className="sv-row">
                  <div className="sv-column">
                    <label>
                      <input
                        type="password"
                        name="senha"
                        placeholder="Senha"
                        onChange={e => {
                          this._handleChange(e);
                        }}
                      />
                    </label>
                  </div>
                </div>
                <a className="link link-info small sv-text-small">Esqueceu a senha?</a>
                <button className="sv-button info full" onClick={e => this._handleLogin(e)}>
                  Login
                </button>
              </form>
            </section>
          </div>
        </div>
      </div>
    );
  }
}
