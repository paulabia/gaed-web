import React from 'react';
import ReactDOM from 'react-dom';
import Axios from './config/Axios';

import AuthState from './states/AuthState';

import App from './App';

AuthState.onLocalLogin();

Axios.interceptors.response.use(
  response => {
    return response;
  },
  error => {
    return new Promise((resolve, reject) => {
      if (
        error.response.status === 401 &&
        error.response.data.error_description === 'The access token provided has expired.'
      ) {
        AuthState.onRefreshToken({ initialRequest: error.config, resolve: resolve, reject: reject });
      } else if (error.response.status === 401 && error.response.data.error === 'unauthorized') {
        AuthState.onLogout();
      } else {
        reject(error);
      }
    });
  }
);

ReactDOM.render(<App />, document.getElementById('app'));
