# GAED FrontEnd

## Para executar as páginas será necessário instalar as seguintes ferramentas:

[NodeJS (versão 6.9 ou superior) e NPM](https://nodejs.org/en/download/)

[Webpack](https://webpack.github.io/docs/installation.html)

[Yarn](https://yarnpkg.com)

É necessário realizar o build do frontend da aplicação. 
No diretório gaed-web, execute o comando abaixo para instalar todas as dependências do frontend:

` npm install`

Agora é possível fazer o build do frontend, através do comando a seguir:

`yarn start`

Para acessar aplicação será na url [http://localhost:3000](http://localhost:3000)



