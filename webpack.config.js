const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanObsoleteChunks = require('webpack-clean-obsolete-chunks');

let path = require('path');

const ENV = process.env.NODE_ENV || 'development';

const VERSION = JSON.stringify(require('./package.json').version).replace(/["']/g, '');

module.exports = env => {
  console.log(
    `|******************* NODE_ENV: ${ENV.toUpperCase()} #### VERSION: ${VERSION} ***********************************|`
  );

  const addPlugin = (add, plugin) => (add ? plugin : undefined);
  const ifProd = plugin => addPlugin(env.prod, plugin);
  const removeEmpty = array => array.filter(i => !!i);
  return {
    entry: {
      application: path.join(__dirname, 'src/index.js'),
      vendor: ['react', 'react-dom', 'react-router', 'react-router-dom'],
    },
    output: {
      filename: '[name].[hash].js',
      path: path.join(__dirname, 'dist/'),
    },
    devtool: env.prod ? 'source-map' : 'eval',
    resolve: {
      alias: {
        config: path.join(__dirname, 'src/config', ENV),
        react: path.resolve('./node_modules/react'),
      },
    },
    devServer: {
      port: 3000,
      compress: true,
      historyApiFallback: true,
    },
    bail: env.prod,
    module: {
      loaders: [
        {
          test: /\.js$|\.jsx$/,
          exclude: /node_modules/,
          loader: 'babel-loader',
          query: {
            plugins: ['transform-decorators-legacy'],
            presets: ['es2015', 'stage-0', 'react'],
          },
        },
        { test: /\.css$/, loader: 'style-loader!css-loader' },
      ],
    },
    externals: {
      'react/addons': true,
      'react/lib/ExecutionEnvironment': true,
      'react/lib/ReactContext': true,
    },
    plugins: removeEmpty([
      new CleanObsoleteChunks(),
      new webpack.HotModuleReplacementPlugin(),
      new HtmlWebpackPlugin({
        template: path.join(__dirname, 'src/index.html'),
        filename: 'index.html',
        inject: 'body',
      }),
      new CopyWebpackPlugin([
        // Copy directory contents to {output}/to/directory/
        { from: path.join(__dirname, 'src/assets') },
      ]),
      ifProd(
        new webpack.LoaderOptionsPlugin({
          minimize: true,
          debug: false,
          quiet: true,
        })
      ),
      ifProd(
        new webpack.DefinePlugin({
          'process.env': {
            NODE_ENV: '"production"',
          },
        })
      ),
      ifProd(
        new webpack.optimize.UglifyJsPlugin({
          compress: {
            screw_ie8: true,
            warnings: false,
          },
        })
      ),
      ifProd(
        new CompressionPlugin({
          asset: '[path].gz[query]',
          algorithm: 'gzip',
          test: /\.js$|\.css$/,
          threshold: 10240,
          minRatio: 0.8,
        })
      ),
    ]),
  };
};
